# Personal site

Photography and robotics-focused side projects displayed on a site created using
Pelican for static site generation, and hosted using gitlab pages.
